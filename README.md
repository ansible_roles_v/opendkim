OpenDKIM
=========
Installs OpenDKIM. 
File *dkim.txt* will be copied to the ansible host.  

Include role
------------
```yaml
- name: opendkim  
  src: https://gitlab.com/ansible_roles_v/opendkim/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - opendkim
```